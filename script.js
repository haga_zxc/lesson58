'use strict';

function  showSplashScreen(){
  let x = document.getElementById("page-splash");
  x.style.visibility = "visible";
  let y = document.getElementById('body');
  y.classList.add("no-scroll");
  
}

function  hideSplashScreen(){
  let x = document.getElementById("page-splash");
  let y = document.getElementById('body');
  x.style.visibility = "hidden"
  y.classList.remove("no-scroll");    
}

let com = {
  id: '1',
  userId: '1',
  postId: '1',
  name : 'someusername',
  text : 'SOME TEXT HERE'
};

let post = {
  id: '1',
  userId : '2',
  image : "https://klike.net/uploads/posts/2019-07/1564314090_3.jpg"
};

function createCommentElement(comment){
  let div = document.createElement('div');
  div.innerHTML += "<a href=# class=muted>" + comment.name + "</a>";
  div.innerHTML += "<p>" + comment.text + "</p>";
  div.classList.add("py-3","pl-3");
  return div;
}

function createPostElement(post){
  let div = document.createElement('div');
  const html = `
  <div>
    <img class="d-block w-100" src="${post.image}" alt="Post image">
  </div>
  
  <div class="px-4 py-3">
    <div class="d-flex justify-content-around">
      <span class="h1 mx-2 text-danger">
        <i class="fas fa-heart"></i>
      </span>
      <span class="h1 mx-2 muted">
        <i class="far fa-heart"></i>
      </span>
      <span class="h1 mx-2 muted">
        <i class="far fa-comment"></i>
      </span>
      <span class="mx-auto"></span>
      <span class="h1 mx-2 muted">
        <i class="far fa-bookmark"></i>
      </span>
      <span class="h1 mx-2 muted">
        <i class="fas fa-bookmark"></i>
     </span>
    </div>
  <hr>
  <div>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
  </div>
  <hr>
  <div id = "comment">
    <div class="py-2 pl-3">
      <a href="#" class="muted">someusername</a>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
    </div>
  <div class="py-2 pl-3">
      <a href="#" class="muted">someusername</a>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
  </div>
  </div>
  </div>
  `;
  div.innerHTML +=html; 
  div.classList.add("card", "my-3");
  return div;
}

let new_post = createPostElement(post);

function addPost(x){
  document.getElementsByClassName('posts-container')[0].append(x);
}

let like = document.getElementById('like');
like.addEventListener('click', function(){
  let heart = like.children[0];
 if(heart.classList.contains("text-danger")){
    heart.classList.remove("text-danger","fas");
 }else{
   heart.classList.add("text-danger","fas");
 }
}); 

let save = document.getElementById('save');
save.addEventListener('click', function(){
  let save_icon = save.children[0];
 if(save_icon.classList.contains("fas")){
  save_icon.classList.remove("fas");
 }else{
  save_icon.classList.add("fas");
 }
}); 

let image_1 = document.getElementsByClassName("image-1")[0];
console.log(image_1);
image_1.addEventListener('dblclick',function(){
  let heartByImage = like.children[0];
  if(heartByImage.classList.contains("text-danger")){
    heartByImage.classList.remove("text-danger","fas");
 }else{
  heartByImage.classList.add("text-danger","fas");
 }
});